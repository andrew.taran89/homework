# Homework

## Pre-installation

Add env variables

backend package
copy .example.env -> .env

frontend package
copy .example.env -> .env

## Running the app

Docker-compose development env

docker-compose -f docker-compose.yml up --build

## Deploy

docker-compose -f docker-compose.prod.yml up --build
