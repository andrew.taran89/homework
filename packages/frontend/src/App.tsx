import React, { ChangeEvent, FormEvent, useState } from "react";
import { API_ENDPOINT } from "./config";
import { ReactComponent as UploadIcon } from "./components/Icons/upload.svg";
import { ReactComponent as InfoIcon } from "./components/Icons/info.svg";

const MAX_COLLECTION_AMOUNT = 10000;

function App() {
  const [pictureFile, setPictureFile] = useState<File>();
  const [picturePreview, setPicturePreview] = useState<string>();
  const [blockchain, setBlockchain] = useState("");
  const [storage, setStorage] = useState("");
  const [collectionName, setCollectionName] = useState<string>("");
  const [collectionSymbol, setCollectionSymbol] = useState<string>("");
  const [collectionAmount, setCollectionAmount] = useState<number>();
  const [owner, setOwner] = useState<string>("");
  const [description, setDescription] = useState<string>("");

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("picture", pictureFile as File);
    formData.append("blockchain", blockchain);
    formData.append("storage", storage);
    formData.append("name", collectionName);
    formData.append("symbol", collectionSymbol);
    formData.append("amount", (collectionAmount as number).toString());
    formData.append("owner", owner);
    formData.append("description", description);

    fetch(`${API_ENDPOINT}/collection`, {
      method: "POST",
      body: formData,
    });
  };

  const handleChangePicture = (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.currentTarget.files?.[0];
    const reader = new FileReader();

    reader.onload = () => {
      setPicturePreview(reader.result as string);
      setPictureFile(file);
    };

    reader.onerror = (error) => {
      console.error("File could not be read: " + error);
    };

    if (file) {
      reader.readAsDataURL(file);
    }
  };

  const handleChangeBlockchain = (e: ChangeEvent<HTMLSelectElement>) => {
    setBlockchain(e.currentTarget.value);
  };

  const handleChangeStorage = (e: ChangeEvent<HTMLSelectElement>) => {
    setStorage(e.currentTarget.value);
  };

  const handleChangeCollectionName = (e: ChangeEvent<HTMLInputElement>) => {
    setCollectionName(e.currentTarget.value);
  };

  const handleChangeCollectionSymbol = (e: ChangeEvent<HTMLInputElement>) => {
    setCollectionSymbol(e.currentTarget.value);
  };

  const handleChangeCollectionAmount = (e: ChangeEvent<HTMLInputElement>) => {
    setCollectionAmount(Number(e.currentTarget.value));
  };

  const handleChangeOwner = (e: ChangeEvent<HTMLInputElement>) => {
    setOwner(e.currentTarget.value);
  };
  const handleChangeDescription = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setDescription(e.currentTarget.value);
  };

  return (
    <div className="p-4">
      <h2 className="text-4xl font-extrabol">Complete informations</h2>
      <p className="my-4 text-lg text-gray-500 max-w-[800px] mb-16">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat.
      </p>
      <form
        onSubmit={handleSubmit}
        className="grid grid-cols-[repeat(auto-fill,minmax(200px,300px))] gap-6 items-end"
      >
        <div className="flex items-center gap-4 col-start-1 col-end-4">
          {picturePreview ? (
            <img
              title="Click to delete"
              className="w-36 h-36 border border-gray-300 rounded-md"
              src={picturePreview}
              alt="Collection"
            />
          ) : (
            <div className="w-36 h-36 bg-gray-50 border border-gray-300 rounded-md flex items-center justify-center">
              <UploadIcon className="w-6 h-6" />
            </div>
          )}
          <label
            htmlFor="picture"
            className="cursor-pointer text-sm font-medium text-gray-900 relative"
          >
            Collection Picture *
            <button
              type="button"
              className="pointer-events-none mt-2 block text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-md text-sm px-4 py-2"
            >
              Upload
            </button>
            <input
              className="rounded-sm opacity-0 absolute bottom-0 w-full"
              required
              type="file"
              id="picture"
              onChange={handleChangePicture}
            />
          </label>
        </div>

        <div className="col-start-1 flex flex-col">
          <label
            htmlFor="blockchain"
            className="mb-2 text-sm font-medium text-gray-900 flex justify-between items-center"
          >
            Blockchain *
            <InfoIcon className="w-5 h-5 cursor-not-allowed" />
          </label>
          <select
            id="blockchain"
            value={blockchain}
            required
            onChange={handleChangeBlockchain}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5"
          >
            <option disabled value="">
              --Select--
            </option>
            <option value="ETH">ETH</option>
            <option value="Polygon">Polygon</option>
          </select>
        </div>

        <div className="flex flex-col">
          <label
            htmlFor="storage"
            className="mb-2 text-sm font-medium text-gray-900 flex justify-between items-center"
          >
            Save my data on *
            <InfoIcon className="w-5 h-5 cursor-not-allowed" />
          </label>
          <select
            id="storage"
            value={storage}
            required
            onChange={handleChangeStorage}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5"
          >
            <option disabled value="">
              --Select--
            </option>
            <option value="IPFS">IPFS</option>
            <option value="Arweave">Arweave</option>
          </select>
        </div>

        <div className="col-start-1 flex flex-col">
          <label
            htmlFor="collectionName"
            className="block mb-2 text-md font-medium text-gray-900"
          >
            Name of the collection *
          </label>
          <input
            type="text"
            id="collectionName"
            value={collectionName}
            required
            onChange={handleChangeCollectionName}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 "
            placeholder="Enter a name"
          ></input>
        </div>

        <div className="flex flex-col">
          <label
            htmlFor="collectionSymbol"
            className="block mb-2 text-sm font-medium text-gray-900"
          >
            Symbol of the collection *
          </label>
          <input
            type="text"
            id="collectionSymbol"
            value={collectionSymbol}
            onChange={handleChangeCollectionSymbol}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 "
            placeholder="Enter a name"
            required
          ></input>
        </div>

        <div className="flex flex-col">
          <label
            htmlFor="collectionAmount"
            className="block mb-2 text-sm font-medium text-gray-900"
          >
            Amount of NFTS in the collection *
          </label>
          <div className="flex">
            <input
              type="number"
              min={1}
              max={MAX_COLLECTION_AMOUNT}
              id="collectionAmount"
              value={collectionAmount}
              required
              onChange={handleChangeCollectionAmount}
              className="bg-gray-50 flex-grow border border-r-0 border-gray-300 text-gray-900 text-sm rounded-l-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 "
              placeholder="Enter an amount"
            ></input>
            <button
              disabled
              className="flex-shrink-0 z-10 inline-flex items-center py-2.5 px-4 text-sm font-medium text-center text-gray-900 bg-gray-100 border border-gray-300 rounded-r-lg hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100"
              type="button"
            >
              Max: {MAX_COLLECTION_AMOUNT}
            </button>
          </div>
        </div>

        <div className="col-start-1 flex flex-col">
          <label
            htmlFor="owner"
            className="mb-2 text-md font-medium text-gray-900 flex justify-between items-center"
          >
            Owner *
            <InfoIcon className="w-5 h-5 cursor-not-allowed" />
          </label>
          <input
            type="text"
            id="owner"
            value={owner}
            required
            onChange={handleChangeOwner}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 "
            placeholder="Enter an address"
          ></input>
        </div>

        <div className="mb-2 max-w-fit cursor-not-allowed text-md font-medium border-b-2 border-gray-300 text-gray-900">
          More options
        </div>

        <div className="col-start-1 col-end-3 flex flex-col">
          <label
            htmlFor="description"
            className="block mb-2 text-md font-medium text-gray-900"
          >
            Description *
          </label>
          <textarea
            id="description"
            value={description}
            required
            rows={6}
            onChange={handleChangeDescription}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 "
            placeholder="Enter a description"
          ></textarea>
        </div>

        <button
          type="submit"
          className="col-start-1 col-end-4 text-white bg-gray-800 hover:bg-gray-900 focus:outline-none focus:ring-4 w-min focus:ring-gray-300 font-medium rounded-lg text-md px-5 py-3"
        >
          Continue
        </button>
      </form>
    </div>
  );
}

export default App;
