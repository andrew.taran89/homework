import {
  IsEnum,
  IsEthereumAddress,
  IsInt,
  IsNotEmpty,
  IsString,
  Max,
  Min,
} from 'class-validator';

export enum Blockchain {
  ETH = 'ETH',
  Polygon = 'Polygon',
}

export enum Storage {
  IPFS = 'IPFS',
  Arweave = 'Arweave',
}

export class CreateCollectionDto {
  picture: Express.Multer.File;

  @IsEnum(Blockchain)
  blockchain: Blockchain;

  @IsEnum(Storage)
  storage: Storage;

  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  symbol: string;

  @IsInt()
  @Min(1)
  @Max(10000)
  amount: number;

  @IsString()
  @IsEthereumAddress()
  owner: string;

  @IsString()
  @IsNotEmpty()
  description: string;
}
