import {
  MaxFileSizeValidator,
  FileTypeValidator,
  ParseFilePipe,
} from '@nestjs/common';
import {
  COLLECTION_PICTURE_TYPE_REGEXP,
  MAX_COLLECTION_PICTURE_SIZE_MB,
} from 'src/constants';

export const PictureFilePipe = new ParseFilePipe({
  validators: [
    new MaxFileSizeValidator({
      maxSize: MAX_COLLECTION_PICTURE_SIZE_MB * 1000 * 1000,
    }),
    new FileTypeValidator({ fileType: COLLECTION_PICTURE_TYPE_REGEXP }),
  ],
});
