import {
  Body,
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { PictureFilePipe } from './collection.pipes';
import { CollectionService } from './collection.service';
import { CreateCollectionDto } from './create-collection.dto';

@Controller('collection')
export class CollectionController {
  constructor(private collectionService: CollectionService) {}

  @Post()
  @UseInterceptors(FileInterceptor('picture'))
  createCollection(
    @Body() body: CreateCollectionDto,
    @UploadedFile(PictureFilePipe)
    picture: Express.Multer.File,
  ) {
    this.collectionService.create({ ...body, picture });
  }
}
