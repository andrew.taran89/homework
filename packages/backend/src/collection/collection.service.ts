import { Model } from 'mongoose';
import { Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { S3Service } from 'src/storage/s3.service';
import { Storage } from 'src/storage/storage.interface';
import { Collection, CollectionDocument } from './collection.schema';
import { CreateCollectionDto } from './create-collection.dto';

@Injectable()
export class CollectionService {
  constructor(
    @Inject(S3Service) private storage: Storage,
    @InjectModel(Collection.name)
    private collectionModel: Model<CollectionDocument>,
  ) {}

  async create(collectionDto: CreateCollectionDto) {
    const picture = await this.storage.upload(collectionDto.picture);

    const createdCollection = new this.collectionModel({
      ...collectionDto,
      picture,
    });

    return createdCollection.save();
  }
}
