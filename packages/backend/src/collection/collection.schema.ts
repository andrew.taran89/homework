import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CollectionDocument = Collection & Document;

@Schema()
export class Collection {
  @Prop()
  picture: string;

  @Prop()
  blockchain: string;

  @Prop()
  storage: string;

  @Prop()
  name: string;

  @Prop()
  symbol: string;

  @Prop()
  amount: number;

  @Prop()
  owner: string;

  @Prop()
  description: string;
}

export const CollectionSchema = SchemaFactory.createForClass(Collection);
