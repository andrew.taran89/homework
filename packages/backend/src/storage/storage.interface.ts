export interface Storage {
  upload(file: Express.Multer.File): Promise<string>;
}
