import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Storage } from './storage.interface';
import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';
import { v4 as uuid } from 'uuid';

const s3Client = new S3Client({});

@Injectable()
export class S3Service implements Storage {
  constructor(private configService: ConfigService) {}

  async upload(file: Express.Multer.File) {
    const bucketName = this.configService.get<string>('AWS_S3_BUCKET_NAME');
    const bucketRegion = this.configService.get<string>('AWS_REGION');

    const command = {
      Bucket: bucketName,
      Key: uuid(),
      Body: file.buffer,
      ContentType: file.mimetype,
    };

    await s3Client.send(new PutObjectCommand(command));

    const pictureUrl = `https://${bucketName}.s3.${bucketRegion}.amazonaws.com/${command.Key}`;

    return pictureUrl;
  }
}
