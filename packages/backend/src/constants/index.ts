export const MAX_COLLECTION_PICTURE_SIZE_MB = 100;
export const COLLECTION_PICTURE_TYPE_REGEXP = /(jpg|jpeg|png)$/;
